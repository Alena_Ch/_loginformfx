package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {

    @FXML
    private Button registerBtn;

    @FXML
    private PasswordField pass;

    @FXML
    private PasswordField pass2;

    @FXML
    private TextField user;

    @FXML
    private Label status;

    public void onClick_Register(ActionEvent event) throws Exception {
        String data = user.getText();
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcher = pattern.matcher(data);
        boolean found = matcher.matches();

        if (found && pass.getText().equals(pass2.getText())) {
            status.setText("Login success");


            Stage ps = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("window.fxml"));
            ps.setTitle("main window");
            ps.setScene(new Scene(root, 400, 400));
            ps.show();

        } else {
            status.setText("Login is failed");
        }
    }

    @FXML
    void initialize() {

    }
}

